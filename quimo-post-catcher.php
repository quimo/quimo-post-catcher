<?php

/**
 * Plugin Name: Quimo Post Catcher
 * Plugin URI: https://bitbucket.org/quimo/quimo-post-catcher/src/master/
 * Description: Imposta i parametri di WP_Query e mappa i campi del resultset su uno specifico template
 * Version: 1.0
 * Author: Simone Alati
 * Author URI: https://www.simonealati.it
 */ 

/*
██████╗  ██████╗ ███████╗████████╗     ██████╗ █████╗ ████████╗ ██████╗██╗  ██╗███████╗██████╗
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝    ██╔════╝██╔══██╗╚══██╔══╝██╔════╝██║  ██║██╔════╝██╔══██╗
██████╔╝██║   ██║███████╗   ██║       ██║     ███████║   ██║   ██║     ███████║█████╗  ██████╔╝
██╔═══╝ ██║   ██║╚════██║   ██║       ██║     ██╔══██║   ██║   ██║     ██╔══██║██╔══╝  ██╔══██╗
██║     ╚██████╔╝███████║   ██║       ╚██████╗██║  ██║   ██║   ╚██████╗██║  ██║███████╗██║  ██║
╚═╝      ╚═════╝ ╚══════╝   ╚═╝        ╚═════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Post%20Catcher

[pc taxonomy="genre|product_cat" term="donna,bambino|mocassini,snickers" template="scarpe"]
[pc taxonomy="product_tag" term="ufficio" template="ufficio" debug="1"]
[pc post_type="page" taxonomy="category" term="homepage" orderby="rand" posts="8" template="cabi-box-pages"]

*/

// termino l'esecuzione se il plugin è richiamato direttamente
if (!defined('WPINC')) die; 

// includo le classi
include_once plugin_dir_path( __FILE__ ) . 'includes/class-quimo-field-mapper.php';
include_once plugin_dir_path( __FILE__ ) . 'includes/class-quimo-post-catcher.php';

// imposto il percorso per i template
define('POST_CATCHER_TEMPLATE_FOLDER', plugin_dir_path( __FILE__ ) . 'assets/templates/');

// creo lo shortcode
add_action('init', function() {
    add_shortcode('pc', 'postCatcherShortcode');
});

// recupero i post via ajax
function post_catcher_get_post() {
    $args = $_REQUEST['args'];
    $paged = $_REQUEST['paged'];
    $args['paged'] = $paged;
    $fm = new FieldMapper();
    if ($fm->get($args)) echo $fm->render('demo');
    else echo false;
    wp_die();
}

// azioni ajax
add_action('wp_ajax_nopriv_post_catcher_get_post', 'post_catcher_get_post');
add_action('wp_ajax_post_catcher_get_post', 'post_catcher_get_post');
add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script('init', plugin_dir_url( __FILE__ ) . '/assets/js/init.js', array('jquery'), mt_rand(), true);
    wp_localize_script('init', 'init_ajax', array('url' => admin_url('admin-ajax.php')));
});

if (!function_exists('postCatcherShortcode')) {
    function postCatcherShortcode($atts, $content = null) {
        // estraggo i parametri
        extract(shortcode_atts(array(
            'post_type' => 'post',
            'taxonomy' => '',
            'term' => '',
            'template' => 'demo',
            'orderby' => 'menu_order date',
            'posts' => -1,
            'relation' => 'AND',
            'filters' => '',
            'paged' => 1,
            'debug' => 0,
            'pagination' => true,
            'quick' => false
        ),
        $atts, 'pc'));

        // recupero i dati
        if (class_exists('PostCatcher')) {
            
            $pc = new PostCatcher();
            $args = array();

            /* seleziono il tipo di post e l'ordinamento */
            $args['post_type'] = $post_type;
            $args['orderby'] = $orderby;
            
            /* se filtro per tassonomia */
            if ($taxonomy) $args = $pc->wpQueryArgsComposer($taxonomy, $term, $posts, $relation, $debug);

            /* se filtro per valore */
            if ($filters) {
                $filters = $pc->wpQueryFiltersComposer($filters);
                $args['meta_query'] = $filters;
            }

            /* aggiungo la paginazione */
            if ($pagination === true) {
                $args['paged'] = $paged;
                $args['posts_per_page'] = $posts;
            } else {
                $args['nopaging'] = true;
                $args['no_found_rows'] = true;
                $args['posts_per_page'] = -1;
            }
            
        }

        // mappo i risultati
        if (class_exists('FieldMapper')) {
            $fm = new FieldMapper();
            if ($quick === false) $fm->get($args,$debug);
            else $fm->get_quick($args,$debug);
            $json = json_encode($args);
            ?><script>
            /* Post Catcher query args */
            var post_catcher_page = 2;
            var post_catcher_query = <?php echo $json ?>
            </script><?php
            $html = $fm->render($template, $debug);
            if ($pagination === true) {
                $load_more = '<div class="post_catcher_load_more">LOAD</div>';
                $html .= $load_more;
            }
            return $html;
        } else {
            var_dump($args);
            return "Bisogna installare ed attivare il plugin field-mapper!";
        }
    }
}

