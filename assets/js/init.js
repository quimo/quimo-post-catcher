jQuery(document).ready(function($) {
    if ($('.post_catcher_load_more').length) {
        $('.post_catcher_load_more').on('click', function() {
            var post_catcher_load_more_label = $('.post_catcher_load_more').html(); 
            $('.post_catcher_load_more').html('<img style="width: 32px" src="' + window.location.origin + '/wp-content/plugins/quimo-post-catcher/assets/images/spinner.gif">');
            jQuery.ajax({
                type: "post",
                url: init_ajax.url,
                data: { 
                  action: 'post_catcher_get_post',
                  args: post_catcher_query,
                  paged: post_catcher_page
                }
            })
            .done(function(response) {
                if (response != false) {
                    $('.post_catcher_load_more').html(post_catcher_load_more_label);
                    /* salvo il pulsante */
                    var load_more = $('.post_catcher_load_more');
                    var item_class = load_more.prev().attr('class');
                    /* lo rimuovo mantenendo il comportamento sul click */
                    load_more.after(response).detach();
                    /* riaccodo il pulsante dopo l'ultimo resultset */
                    $('.' + item_class).last().after(load_more);
                    /* mi sposto nalla paginazione */
                    post_catcher_page++;
                } else {
                    $('.post_catcher_load_more').hide();
                }
            })
            .fail(function(){
                console.log('error');
            });

        });
    }
});