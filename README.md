# Quimo Post Catcher #

Plugin che consente di recuperare i post di WordPress (post, pagine e custom post) impostando dei parametri tassonomici e di mappare i risultati su un template specifico attraverso l'uso di placeholder.

### Demo: https://www.simonealati.it/post-catcher-demo/ ###

### Come si usa? ###

Quimo Post Catcher si può utilizzare nell'editor di WordPress con il relativo shortcode `[pc]` o in un qualsiasi file PHP del tema in uso. Nel primo caso c'è un numero prefissato di parametri utilizzabili per estarre i post mentre nel secondo si fa riferimento direttamente agli argomenti di WP_Query.

### Quimo Post Catcher nell'editor di WordPress ###

`[pc template="file_html"]`

Si richiama lo shortcode con i seguenti parametri...

* `post_type`: tipo di post (es. page, post, product - vedi WP_Query)
* `taxonomy`: elenco di tassonomie divise da "|" es. taxonomy1|taxonomy2
* `term`: elenco di termini separati da "," (se nella stessa tassonomia) o da "|" se di tassonomie diverse es. term1_taxonomy1,term2_taxonomy1|term1_taxonomy2
* `filters`: elenco di termini *chiave, confronto, valore* separati da "|"
* `orderby`: ordinamento (default *menu_order date* - per un'estrazione casuale si può usare *rand*)
* `posts`: numero di post da recuperare (default 8 - per recupararli tutti si può usare -1)
* `template`: file HTML nella root del tema su cui mappare il resultset 
* `debug`: flag di debug (default 0)

In questo esempio *genre* è una tassonomia custom e *product_cat* è la tassomonia "categoria" di default per i prodotti di WooCommerce. Recupero i post che nella tassonomia *genre* hanno il termine *donna* o *bambino* e nella tassonomia *product_cat* il termine *mocassini* o *snikers*. I risultati sono mappati sul file scarpe.html:

`[pc taxonomy="genre|product_cat" term="donna,bambino|mocassini,snickers" template="scarpe"]`

In questo esempio *product_tag* è la tassomonia "tag" di default per i prodotti di WooCommerce. Recupero i post che nella tassonomia *product_tag* hanno il termine *ufficio*. I risultati sono mappati sul file ufficio.html:

`[pc taxonomy="product_tag" term="ufficio" template="ufficio" debug="1"]`

In questo esempio recupero 6 pagine random che appartengono alla categoria *homepage*. I risultati sono mappati sul file pages.html:

`[pc post_type="page" taxonomy="category" term="homepage" orderby="rand" posts="6" template="pages"]`

Altri esempi:

`[pc template="demo" post_type="page"  orderby="rand" posts=2]`

`[pc template="demo" taxonomy="category" term="geek"]`

`[pc template="demo" taxonomy="category|category" term="geek|tools" relation="OR"]`

`[pc template="demo" taxonomy="post_tag" term="cf7"]`

`[pc template="demo" post_type="page" filters="custom_field_1,like,valore|custom_field_2,eq,5"]`

Il parametro *confronto* usato qua sopra indica le condizioni di ricerca sui campi:

* lk o like: LIKE
* eq: =
* gt: >
* ge: >=
* lt: <
* le: <=

### Quimo Post Catcher nei file PHP ###

Uso la classe *fieldMapper* dopo aver impostato gli argomenti per WP_Query:

```php
$args = array(
    'post_type' => 'portfolio',
    'nopaging' => 'true',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC'
);
$fm = new fieldMapper();
echo $fm->show($args, 'portfolio');
```

In questo esempio sto mappando il custom post type *portfolio* sul file `portfolio.html`.

### I template ###

I template che visuallizzano i risultati sono file HTML, da salvare nella *root* del tema, con placeholder al posto dei valori estratti da WP_Query:

```html
<div class="slide">
    <a href="[+permalink+]">
    <img src="[+full+]" alt="">
    </a>
</div>
```

Sono disponibili anche i campi creati con ACF e relativi repeater:

```
[+ci_hanno_scelto+] <!-- campo ACF repeater (array) -->
<div class="testimonial">
	<h2>[+brand_name+]</h2> <!-- campo ACF -->
</div>
[+/ci_hanno_scelto+]
```

I placeholder utilizzabili sono:

* `[+title+]`: titolo
* `[+content+]`: contenuto
* `[+content_notag+]`: contenuto senza tag HTML
* `[+excerpt+]`: riassunto
* `[+permalink+]`: link
* `[+thumbnail+]`: immagine *thumbnail*
* `[+medium+]`: immagine *medium*
* `[+large+]`: immagine *large*
* `[+full+]`: immagine *full*
* campi "semplici" ACF (no array / oggetti) o repeater ACF
* `[+price+]`: prezzo di un prodotto WooCommerce