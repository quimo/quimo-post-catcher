<?php

class PostCatcher {

    public function wpQueryArgsComposer($taxonomies, $terms, $posts, $relation, $debug) {
        if (strpos($taxonomies, '|') !== false && strpos($terms, '|') !== false) {
            /* ho valori multipli per tassomonia (e termini) */
            $taxonomies = explode('|', $taxonomies);
            $terms = explode('|', $terms);
            if (count($taxonomies) != count($terms)) return false;
            /* inserisco i vincoli tassonomia = termine/i */
            $tax_query = array('relation' => $relation);
            for ($i = 0; $i < count($taxonomies); $i++) {
                $tax_query[] = array(
                    'taxonomy' => $taxonomies[$i],
            		'field' => 'slug',
            		'terms' => $this->explodeTerms($terms[$i]),
                );
            }
            $args = array(
                'posts_per_page' => $posts,
                'tax_query' => $tax_query,
            );
			if ($debug) $this->debug($args);
            return $args;
        } elseif (strpos($taxonomies, '|') === false && strpos($terms, '|') === false) {
            /* ho una sola tassomonia (e termine) */
            $args = array(
                'posts_per_page' => $posts,
                'tax_query' => array(
                	array(
                		'taxonomy' => $taxonomies,
                		'field' => 'slug',
                		'terms' => $this->explodeTerms($terms)
                	),
                )
            );
            if ($debug) $this->debug($args);
            return $args;
        } else {
            /* tassonomie e termini non corrispondono */
			if ($debug) echo "Tassonomie e termini non corrispondono";
            return false;
        }
    }

    /* TODO = DA COMPLETARE
     * funziona solo per i campi aggiiuntivi nella tabella dei meta
     * se voglio filrare per i campi base del post devo usare il
     * filtro posts_where
    */
    public function wpQueryFiltersComposer($filters) {
        if (strpos($filters, '|') !== false) {
            $args = array('relation' => 'AND');
            /* ho valori multipli per i filtri */
            $filters = explode('|', $filters);
            for ($i = 0; $i < count($filters); $i++) {
                list($key, $compare, $value) = explode(',', $filters[$i]);
                $args[] = array(
                    'key' => $key,
                    'value' => $value,
                    'compare' => $this->compare($compare, 'compare'),
                    'type' => $this->compare($compare, 'type')
                );
                
            }
            
        } else {
            $args = array();
            /* ho un solo filtro */
            list($key, $compare, $value) = explode(',', $filters);
            $args[] = array(
                'key' => $key,
                'value' => $value,
                'compare' => $this->compare($compare, 'compare'),
                'type' => $this->compare($compare, 'type')
            );
        }
        return $args;

    }

    private function compare($compare, $type) {
        
        if ($type == 'compare') {
            switch ($compare) {
                case 'like':
                case 'lk':
                    return 'LIKE';
                case 'eq':
                    return '=';
                case 'gt':
                    return '>';
                case 'ge':
                    return '>=';
                case 'lt':
                    return '<';
                case 'le':
                    return '<=';
            }
        } else {
            switch ($compare) {
                case 'like':
                case 'lk':
                    return 'CHAR';
                case 'eq':
                    return 'NUMERIC';
                case 'gt':
                    return 'NUMERIC';
                case 'ge':
                    return 'NUMERIC';
                case 'lt':
                    return 'NUMERIC';
                case 'le':
                    return 'NUMERIC';
            }
        }

    }

    private function explodeTerms($term) {
        $terms = (strpos($term, ',') !== false) ? explode(',', $term) : $term;
        return $terms;
    }

	private function debug($args) {
		echo "ARGOMENTI PASSATI A WP_Query<br>";
		var_dump($args);
		echo "<br><br>";
	}

}
