<?php

/**
 * Plugin Name: Quimo Post Catcher
 * Plugin URI: https://bitbucket.org/quimo/quimo-field-mapper/src/master/
 * Description: Mappa i campi del resultset di WP_Query su un specifico template
 * Version: 1.0
 * Author: Simone Alati
 * Author URI: https://www.simonealati.it
 */

/*

███████╗██╗███████╗██╗     ██████╗     ███╗   ███╗ █████╗ ██████╗ ██████╗ ███████╗██████╗
██╔════╝██║██╔════╝██║     ██╔══██╗    ████╗ ████║██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗
█████╗  ██║█████╗  ██║     ██║  ██║    ██╔████╔██║███████║██████╔╝██████╔╝█████╗  ██████╔╝
██╔══╝  ██║██╔══╝  ██║     ██║  ██║    ██║╚██╔╝██║██╔══██║██╔═══╝ ██╔═══╝ ██╔══╝  ██╔══██╗
██║     ██║███████╗███████╗██████╔╝    ██║ ╚═╝ ██║██║  ██║██║     ██║     ███████╗██║  ██║
╚═╝     ╚═╝╚══════╝╚══════╝╚═════╝     ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝     ╚══════╝╚═╝  ╚═╝

http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Field%20mapper

*/

class FieldMapper {

    private $data = array();

    function show ($args, $template, $random = 0, $debug = 0) {
        $this->get($args, $debug);
        if (!empty($this->data)) return $this->render($template, $random, $debug);
        return false;
    }

    public function get_quick($args, $debug = 0) {
        $loop = new WP_Query($args);
        if ($loop->have_posts()) {
			if ($debug) $this->debug('', 'start');
            $i = 0;
            while ($loop->have_posts()) {
                $loop->the_post();
                $id = get_the_ID();
                $this->data[$i]['permalink'] = get_the_permalink($id);
                $this->data[$i]['title'] = get_the_title();
                $this->data[$i]['excerpt'] = nl2br(get_the_excerpt());
                /* immagini */
                if (function_exists('has_post_thumbnail') && has_post_thumbnail($id)) {
				    $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'thumbnail');
				    $this->data[$i]['thumbnail'] = $dummy[0];
                }
				if ($debug) $this->debug($this->data[$i], 'loop');
                $i++;
            }
        }
        wp_reset_query();
        wp_reset_postdata();
    }

    public function get($args, $debug = 0) {
        $loop = new WP_Query($args);
        if ($loop->have_posts()) {
			if ($debug) $this->debug('', 'start');
            $i = 0;
            while ($loop->have_posts()) {
                $loop->the_post();
                $id = get_the_ID();
                $this->data[$i]['permalink'] = get_the_permalink($id);
                $this->data[$i]['title'] = get_the_title();
                $this->data[$i]['content'] = $this->get_content();
                $this->data[$i]['excerpt'] = nl2br(get_the_excerpt());
                $this->data[$i]['content_notag'] = get_the_content();
                /* immagini */
                if (function_exists('has_post_thumbnail') && has_post_thumbnail($id)) {
				    $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'thumbnail');
				    $this->data[$i]['thumbnail'] = $dummy[0];
                    $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'medium');
				    $this->data[$i]['medium'] = $dummy[0];
                    $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'large');
				    $this->data[$i]['large'] = $dummy[0];
                    $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'full');
				    $this->data[$i]['full'] = $dummy[0];
                }
                /* advanced custom fields */
                if (function_exists('get_fields')) {
                    $fields = get_fields($id);
                    if ($fields) {
                        foreach( $fields as $name => $value ) {
                            $this->data[$i][$name] = $value;
                        }
                    }
                }
                /* woocommerce product */
                if ($args['post_type'] == 'product') {
                    if (function_exists('wc_get_product') && wc_get_product($id)) {
                        $product = wc_get_product($id);
                        if ($product) {
                            if ($product->is_type('simple')) {
                                $this->data[$i]['price'] = $product->get_price();
                            } elseif ($product->is_type('variable')) {
                                $price_min = $product->get_variation_price('min');
                                $price_max = $product->get_variation_price('max');
                                $price_range = ($price_min != $price_max) ? number_format((float)$price_min, 2, ',', '') . ' - ' . number_format((float)$price_max, 2, ',', '') : number_format((float)$price_min, 2, ',', '');
                                $this->data[$i]['price'] = $price_range . get_woocommerce_currency_symbol();
                            }
                        }
                    }
                }
				if ($debug) $this->debug($this->data[$i], 'loop');
                $i++;
            }
            return true;
        } else return false;
        wp_reset_query();
        wp_reset_postdata();
    }

    public function render($template, $random = 0, $debug = 0) {
        if ($template) {
            
            // recupero il template
            $tmpl_url = POST_CATCHER_TEMPLATE_FOLDER . $template.'.html';
            $template_content = ($debug) ? file_get_contents($tmpl_url) : @file_get_contents($tmpl_url);

			/* metodo alternativo a file_get_contents
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $tmpl_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$template_content = curl_exec($ch);
			curl_close($ch);
			*/

    		// sostituisco i [+placeholder+]
			if ($debug) $this->debug($tmpl_url, 'template');

    		if ($template_content && !empty($this->data)) {
                $html = '';
                for ($i = 0; $i < count($this->data); $i++) {
                    $row = $template_content;
                    foreach ($this->data[$i] as $key => $value) {
                        if (is_array($value)) {
                            /* se il campo è un array (repeater) recupero la sezione di template da ripetere */
                            if ($random) shuffle($value);
                            $pos = strpos($row, '[+'.$key.'+]');
                            if ($pos !== false) {
                                $section = self::getStringBetween($row, '[+'.$key.'+]', '[+/'.$key.'+]');
                                $row = str_replace('[+'.$key.'+]'.$section.'[+/'.$key.'+]', "", $row);
                                $sub_html = '';
                                for ($j = 0; $j < count($value); $j++) {
                                    /* ciclo nei sottocampi del repeater */
                                    $sub_template = $section;
									if ($value[$j]) {
										foreach ($value[$j] as $sub_name => $sub_value) {
											$sub_template = str_replace("[+".$sub_name."+]",$sub_value,$sub_template);
										}
									}
                                    $sub_html .= $sub_template;
                                }
                                $row = substr_replace($row, $sub_html, $pos, 0);
                            }
                        } else {
                            /* se il campo non è un array */
                            $row = str_replace("[+".$key."+]", $value, $row);
                        }
                    }
                    $html .= $row;
                }
                return $html;
    		}
            return false;
        }
        return false;
    }

    private function get_content($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
        $content = get_the_content($more_link_text, $stripteaser, $more_file);
        //$content = apply_filters('the_content', $content);
    	//$content = str_replace(']]>', ']]&gt;', $content);
    	return $content;
    }

	private function debug($data, $action) {
		switch($action) {
			case 'start':
				?><br>RISULTATI DI WP_Query<br><?php
				break;
			case 'loop':
				?><a href="<?php echo $data['permalink'] ?>"><?php echo $data['title'] ?></a><br><?php
				break;
			case 'template':
				?><br>TEMPLATE SU CUI MAPPARE I RISULTATI<br><?php
				?><a target="_blank" href="<?php echo $data ?>">Clicca qui</a><br><?php
				break;
		}
	}

    private static function getStringBetween($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}
